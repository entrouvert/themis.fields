# -*- coding: utf-8 -*-

from zope import component
from zope.browser.interfaces import ITerms
from zope.interface import implements, classProvides
from zope.schema.interfaces import ISource, IContextSourceBinder

from zope.app.form.browser.interfaces import ISourceQueryView

from zope.schema.interfaces import IVocabularyFactory
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm

from Products.CMFCore.utils import getToolByName

from plone.registry.interfaces import IRegistry
from plone.formwidget.contenttree import ObjPathSourceBinder
from plone.formwidget.contenttree.source import ObjPathSource

try:
    from tabellio.config.interfaces import ITabellioSettings
except ImportError:
    ITabellioSettings = None


class CommissionsSource(object):
    implements(IContextSourceBinder)

    def __init__(self):
        pass

    def __call__(self, context):
        catalog = getToolByName(context, 'portal_catalog')
        results = catalog(portal_type='themis.datatypes.commission')
        commissions = sorted([x.getObject().title for x in results])
        terms = [SimpleVocabulary.createTerm(x, x.encode('ascii', 'replace'), x) for x in commissions]
        return SimpleVocabulary(terms)

    def __iter__(self):
        # hack to let schema editor handle the field
        yield u'DO NOT TOUCH'


def cmp_person(x, y):
    t = cmp(x.lastname.lower(), y.lastname.lower())
    if t: return t
    return cmp(x.firstname.lower(), y.lastname.lower())

def get_terms_for_persons(context, include_deputies=False,
                          include_ministries=False, include_ministries_collaborators=False):
    catalog = getToolByName(context, 'portal_catalog')

    if include_deputies:
        results = catalog(portal_type='themis.datatypes.deputy')
        deputies = [x.getObject() for x in results]
    else:
        deputies = []

    if include_ministries:
        results = catalog(portal_type='themis.datatypes.ministry')
        ministries = [x.getObject() for x in results]
    else:
        ministries = []

    list = []
    if include_deputies:
        list.extend(deputies)
    if include_ministries:
        list.extend(ministries)

    list.sort(cmp_person)

    terms = []
    for person in list:
        if person in deputies:
            prefix = 'deputy'
            if person.polgroup:
                label = '%s %s (%s)' % (person.lastname, person.firstname, person.polgroup.to_object.title)
            else:
                label ='%s %s (?)' % (person.lastname, person.firstname)
        else:
            prefix = 'ministry'
            label = '%s %s (ministre)' % (person.lastname, person.firstname)

        terms.append(SimpleVocabulary.createTerm(
                                '%s:%s' % (prefix, person.id),
                                '%s:%s' % (prefix, person.id),
                                label))

    if include_ministries_collaborators:
        results = catalog(portal_type='themis.datatypes.ministry')
        ministries = [x.getObject() for x in results]
        ministries.sort(cmp_person)
        for person in ministries:
            terms.append(SimpleVocabulary.createTerm(
                                'ministry-collab:%s' % person.id,
                                'ministry-collab:%s' % person.id,
                                'Collaborateur du Ministre %s %s' % (person.firstname, person.lastname)))

    return terms


class DeputiesSource(object):
    implements(IContextSourceBinder)

    def __call__(self, context):
        return SimpleVocabulary(get_terms_for_persons(context, include_deputies=True))

    def __iter__(self):
        # hack to let schema editor handle the field
        yield u'DO NOT TOUCH'


class MinistriesSource(object):
    implements(IContextSourceBinder)

    def __call__(self, context):
        college_term = [SimpleVocabulary.createTerm(
                        'ministry:college','ministry:college', u'Gouvernement')]
        return SimpleVocabulary(get_terms_for_persons(context, include_ministries=True) + college_term)

    def __iter__(self):
        # hack to let schema editor handle the field
        yield u'DO NOT TOUCH'


class DeputiesAndMinistriesSource(object):
    implements(IContextSourceBinder)

    def __init__(self):
        pass

    def __call__(self, context):
        college_term = [SimpleVocabulary.createTerm(
                        'ministry:college','ministry:college', u'Gouvernement')]
        return SimpleVocabulary(get_terms_for_persons(context,
                                include_deputies=True, include_ministries=True) + college_term)

    def __iter__(self):
        # hack to let schema editor handle the field
        yield u'DO NOT TOUCH'


class ContactsVocabulary(SimpleVocabulary):
    context = None

    def __init__(self, context, contact_ids, *args, **kwargs):
        self.context = context
        self.contact_ids = contact_ids
        super(ContactsVocabulary, self).__init__(*args, **kwargs)

    def getTermByToken(self, token):
        try:
            return super(ContactsVocabulary, self).getTermByToken(token)
        except LookupError:
            if not token:
                raise
            if token.startswith('contact:'):
                raise

            # a simple string, create a contact object to match
            portal = getToolByName(self.context, 'portal_url').getPortalObject()
            plone_tool = getToolByName(self.context, 'plone_utils')
            new_id = plone_tool.normalizeString(token)
            if not portal.contacts.has_key(new_id):
                portal.contacts.invokeFactory(
                            'themis.datatypes.contact', new_id, title=token)

            return self.createTerm('contact:'+new_id, 'contact:' + new_id, token)

    def search(self, qs):
        q = qs.lower()
        t = [self.by_value.get('contact:'+kw) for kw in self.contact_ids if q in kw.lower()]
        return t


class ContactsSource(object):
    implements(IContextSourceBinder)

    def __init__(self):
        pass

    def fastGetTitleByToken(self, context, token):
        if not ':' in token:
            return token
        if token == 'ministry:college':
            return u'Gouvernement'
        prefix, value = token.split(':')
        portal = getToolByName(context, 'portal_url').getPortalObject()
        if prefix == 'deputy':
            url = 'deputes'
        elif prefix in ('ministry', 'ministry-collab'):
            url = 'ministres'
        elif prefix == 'contact':
            url = 'contacts'
        else:
            raise KeyError(token)
        try:
            return getattr(getattr(portal, url), value).Title()
        except AttributeError:
            raise KeyError(token)

    def __call__(self, context):
        catalog = getToolByName(context, 'portal_catalog')

        depmin_terms = get_terms_for_persons(context,
                        include_deputies=True, include_ministries=True)
        college_term = [SimpleVocabulary.createTerm(
                        'ministry:college','ministry:college', u'Gouvernement')]

        results = catalog(portal_type='themis.datatypes.contact')
        contacts = [x.getObject() for x in results]
        def cmp_contact(x, y):
            return cmp(x.title, y.title)
        contacts.sort(cmp_contact)
        contact_ids = [x.id for x in contacts]
        contacts_terms = [SimpleVocabulary.createTerm(
                                'contact:'+x.id, 'contact:'+x.id, x.title) for x in contacts]
        return ContactsVocabulary(context, contact_ids, depmin_terms + college_term + contacts_terms)

    def __iter__(self):
        # hack to let schema editor handle the field
        yield u'DO NOT TOUCH'


class SpeakersSource(object):
    implements(IContextSourceBinder)

    def __init__(self):
        pass

    def __call__(self, context):
        extra_terms = [SimpleVocabulary.createTerm(
                        'speaker:cour-des-comptes', 'speaker:cour-des-comptes', u'Cour des comptes'),
                       SimpleVocabulary.createTerm(
                        'speaker:huis-clos', 'speaker:huis-clos', u'Orateur(s) à huis clos')]
        return SimpleVocabulary(get_terms_for_persons(context,
                                include_deputies=True,
                                include_ministries=True,
                                include_ministries_collaborators=True) + extra_terms)

    def __iter__(self):
        # hack to let schema editor handle the field
        yield u'DO NOT TOUCH'



class SubjectsSource(object):
    implements(IContextSourceBinder)

    def __init__(self):

        if ITabellioSettings:
            settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
            topics = settings.topics
            if topics:
                self.terms = [x.strip() for x in topics.splitlines() if x]
                return

        # fallback on static list
        self.terms = [
                u"Action sociale",
                u"Budget",
                u"Budget du PFB",
                u"Cohésion sociale",
                u"Compétences résiduaires",
                u"Compte du PFB",
                u"Coordination de la politique du Collège",
                u"Culture",
                u"Enseignement",
                u"Famille",
                u"Fonction publique / administration",
                u"Formation professionnelle",
                u"Politique d'aide aux personnes handicapées",
                u"Règlement du PFB",
                u"Relations internationales",
                u"Santé",
                u"Sport",
                u"Tourisme",
                u"Transport scolaire",
                ]

    def __call__(self, context):
        terms = [SimpleVocabulary.createTerm(x, x.encode('ascii', 'replace'), x) for x in self.terms]
        return SimpleVocabulary(terms)

    def __iter__(self):
        # hack to let schema editor handle the field
        yield u'DO NOT TOUCH'


class LegislativeSessionsSource(object): # Legislative sessions
    implements(IContextSourceBinder)

    def __init__(self):
        self.terms = None

    def _setup(self):
        if self.terms:
            return

        if ITabellioSettings:
            settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
            sessions = settings.sessions
            if sessions:
                self.terms = [x.split(':')[1].strip() for x in sessions.splitlines() if x]
                return

        # fallback to an hardcoded list
        self.terms = [
                u"2010 - 2011",
                u"2009 - 2010",
                u"Session Ordinaire 2009",
                u"2008 - 2009",
                u"2007 - 2008",
                u"2006 - 2007",
                u"2005 - 2006",
                u"2004 - 2005",
                u"Session Extraordinaire 2004",
                u"2003 - 2004",
                u"2002 - 2003",
                u"2001 - 2002",
                u"2000 - 2001",
                u"1999 - 2000",
                u"Session Extraordinaire 1999",
                u"1998 - 1999",
                u"1997 - 1998",
                u"1996 - 1997",
                u"1995 - 1996",
                u"1994 - 1995",
                u"1993 - 1994",
                u"1992 - 1993",
                u"1991 - 1992",
                u"1990 - 1991",
                u"1989 - 1990",
                ]

    def __call__(self, context):
        self._setup()
        terms = [SimpleVocabulary.createTerm(x, x.encode('ascii', 'replace'), x) for x in self.terms]
        return SimpleVocabulary(terms)

    def __iter__(self):
        # hack to let schema editor handle the field
        yield u'DO NOT TOUCH'

class RelatedDocObjPathSource(ObjPathSource):
    def getTermByBrain(self, brain, real_value=True):
        if real_value:
            value = brain._unrestrictedGetObject()
        else:
            value = brain.getPath()[len(self.portal_path):]
        if type(brain.Title) is unicode:
            title = brain.Title
        else:
            title = unicode(brain.Title, 'utf-8')
        return SimpleTerm(value, token=brain.getPath(), title=u'%s [%s]' % (
                                 title.replace('\n', ' - ').replace('\r', ''),
                                 brain.Type))


class RelatedDocObjPathSourceBinder(ObjPathSourceBinder):
    path_source = RelatedDocObjPathSource

    def __iter__(self):
        # hack to let schema editor handle the field
        yield u'DO NOT TOUCH'
