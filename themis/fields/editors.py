from zope import interface, component, schema
from zope.schema import interfaces as schema_ifaces
from plone.schemaeditor import interfaces as editor_ifaces

from themis.fields import interfaces
from themis.fields import Commission, Commissions
from themis.fields import DateOnly
from themis.fields import RadioChoice
from themis.fields import Subjects
from themis.fields import LegisSession
from themis.fields import Contact, Contacts
from themis.fields import Deputy, Deputies
from themis.fields import Ministry, Ministries
from themis.fields import Speakers
from themis.fields import DeputyOrMinistry, DeputiesOrMinistries
from themis.fields import RelatedDoc
from themis.fields import RelatedDocs
from themis.fields import MailId
from themis.fields import MailRefId
from themis.fields import DocHistoLines
from themis.fields import PreviewDoc
from themis.fields import PointingDocs
from plone.schemaeditor.fields import FieldFactory

@interface.implementer(editor_ifaces.IFieldEditFormSchema)
@component.adapter(schema_ifaces.IList)
def getSubjectsFieldSchema(field):
    return se_schema.ITextLineMultiChoice

CommissionFactory = FieldFactory(Commission, u'Commission')
CommissionsFactory = FieldFactory(Commissions, u'Commissions')

SubjectsFactory = FieldFactory(Subjects, u'Subjects')

ContactFactory = FieldFactory(Contact, u'Contact')
ContactsFactory = FieldFactory(Contacts, u'Contacts')

DeputyFactory = FieldFactory(Deputy, u'Deputy')
DeputiesFactory = FieldFactory(Deputies, u'Deputies')

MinistryFactory = FieldFactory(Ministry, u'Ministry')
MinistriesFactory = FieldFactory(Ministries, u'Ministries')

DeputyOrMinistryFactory = FieldFactory(DeputyOrMinistry, u'Deputy or Ministry')
DeputiesOrMinistriesFactory = FieldFactory(DeputiesOrMinistries, u'Deputies or Ministries')

SpeakersFactory = FieldFactory(Speakers, u'Speakers')

LegisSessionFactory = FieldFactory(LegisSession, u'Legislative Session')

DateOnlyFactory = FieldFactory(DateOnly, u'Date (seule)')

RadioChoiceFactory = FieldFactory(RadioChoice, u'Choice (radio)', values=[])

RelatedDocFactory = FieldFactory(RelatedDoc, u'Related Document')

RelatedDocsFactory = FieldFactory(RelatedDocs, u'Related Documents')

MailRefIdFactory = FieldFactory(MailRefId, u'Mail Reference')

MailIdFactory = FieldFactory(MailId, u'Mail Number')

DocHistoLinesFactory = FieldFactory(DocHistoLines, u'Historic Lines')

PreviewDocFactory = FieldFactory(PreviewDoc, u'Preview Doc')

PointingDocsFactory = FieldFactory(PointingDocs, u'Pointing Docs')

