from zope.interface import implements, implementsOnly
from zope.schema import Choice, Field, List, Date, Orderable, TextLine

from zope.schema.interfaces import IFromUnicode, IChoice
from zope.schema.interfaces import WrongType

from z3c.relationfield.schema import RelationChoice, RelationList

from themis.fields.interfaces import ICommission
from themis.fields.interfaces import ICommissions
from themis.fields.vocabs import CommissionsSource

from themis.fields.interfaces import IDeputy, IDeputies
from themis.fields.vocabs import DeputiesSource

from themis.fields.interfaces import IMinistry, IMinistries
from themis.fields.vocabs import MinistriesSource

from themis.fields.interfaces import IDeputyOrMinistry, IDeputiesOrMinistries
from themis.fields.vocabs import DeputiesAndMinistriesSource

from themis.fields.interfaces import IContact, IContacts
from themis.fields.vocabs import ContactsSource

from themis.fields.interfaces import ISpeakers
from themis.fields.vocabs import SpeakersSource

from themis.fields.interfaces import IDateOnly
from themis.fields.interfaces import IRadioChoice
from themis.fields.interfaces import ISubjects, ILegisSession, IList
from themis.fields.interfaces import IRelatedDoc
from themis.fields.interfaces import IRelatedDocs
from themis.fields.interfaces import IMailId
from themis.fields.interfaces import IMailRefId
from themis.fields.interfaces import IPreviewDoc
from themis.fields.interfaces import IPointingDocs

from themis.fields.interfaces import IDocHistoLine, IDocHistoLines

from themis.fields.vocabs import SubjectsSource
from themis.fields.vocabs import LegislativeSessionsSource
from themis.fields.vocabs import RelatedDocObjPathSourceBinder

from z3c.relationfield.schema import Relation, RelationList


class Commission(Field):
    implements(ICommission, IChoice)

    vocabularyName = 'XXX' # fake attribute, this allows exportmodel to work

    def __init__(self, **kw):
        self.required = False
        self.vocabulary = CommissionsSource()
        for attr in ('vocabulary', 'vocabularyName'):
            if attr in kw: del kw[attr]
        super(Commission, self).__init__(**kw)

    source = property(lambda self: self.vocabulary)

    def bind(self, object):
        """See zope.schema._bootstrapinterfaces.IField."""
        clone = super(Commission, self).bind(object)
        clone.vocabulary = CommissionsSource()(object)
        return clone

    def _validate(self, value):
        super(Commission, self)._validate(value)
        vocabulary = self.vocabulary
        if value not in vocabulary:
            raise ConstraintNotSatisfied(value)

    def fromUnicode(self, str):
        self.validate(str)
        return str


class SourcedChoice(Choice):
    pass


class Subjects(Field):
    implements(ISubjects, IList)
    _type = list

    def __init__(self, **kw):
        self.required = False
        self.value_type = SourcedChoice(source=SubjectsSource())
        self.unique = True
        self.min_length = None
        self.max_length = None

        for attr in ('min_length', 'max_length', 'unique', 'value_type'):
            if attr in kw: del kw[attr]
        super(Subjects, self).__init__(**kw)


class Commissions(Field):
    implements(ICommissions, IList)
    _type = list

    def __init__(self, **kw):
        self.required = False
        self.value_type = SourcedChoice(source=CommissionsSource())
        self.unique = True
        self.min_length = None
        self.max_length = None

        for attr in ('min_length', 'max_length', 'unique', 'value_type'):
            if attr in kw: del kw[attr]
        super(Commissions, self).__init__(**kw)


class Contact(Field):
    implements(IContact, IChoice)

    vocabularyName = 'XXX' # fake attribute, this allows exportmodel to work

    def __init__(self, **kw):
        self.required = False
        self.vocabulary = ContactsSource()
        for attr in ('vocabulary', 'vocabularyName'):
            if attr in kw: del kw[attr]
        super(Contact, self).__init__(**kw)

    source = property(lambda self: self.vocabulary)

    def bind(self, object):
        """See zope.schema._bootstrapinterfaces.IField."""
        clone = super(Contact, self).bind(object)
        clone.vocabulary = ContactsSource()(object)
        return clone

    def _validate(self, value):
        super(Contact, self)._validate(value)
        vocabulary = self.vocabulary
        if value not in vocabulary:
            raise ConstraintNotSatisfied(value)

    def fromUnicode(self, str):
        self.validate(str)
        return str


class Contacts(Field):
    implements(IContacts, IList)
    _type = list

    def __init__(self, **kw):
        self.required = False
        self.value_type = SourcedChoice(source=ContactsSource())
        self.unique = True
        self.min_length = None
        self.max_length = None

        for attr in ('min_length', 'max_length', 'unique', 'value_type'):
            if attr in kw: del kw[attr]
        super(Contacts, self).__init__(**kw)

    def constraint(self, value):
        # it's possible for a value to have been just created and to still be
        # missing from the vocabulary
        return True


class Deputy(Field):
    implements(IDeputy, IChoice)

    vocabularyName = 'XXX' # fake attribute, this allows exportmodel to work

    def __init__(self, **kw):
        self.required = False
        self.vocabulary = DeputiesSource()
        for attr in ('vocabulary', 'vocabularyName'):
            if attr in kw: del kw[attr]
        super(Deputy, self).__init__(**kw)

    source = property(lambda self: self.vocabulary)

    def bind(self, object):
        """See zope.schema._bootstrapinterfaces.IField."""
        clone = super(Deputy, self).bind(object)
        clone.vocabulary = DeputiesSource()(object)
        return clone

    def _validate(self, value):
        super(Deputy, self)._validate(value)
        vocabulary = self.vocabulary
        if value not in vocabulary:
            raise ConstraintNotSatisfied(value)

    def fromUnicode(self, str):
        self.validate(str)
        return str


class Deputies(Field):
    implements(IDeputies, IList)
    _type = list

    def __init__(self, **kw):
        self.required = False
        self.value_type = SourcedChoice(source=DeputiesSource())
        self.unique = True
        self.min_length = None
        self.max_length = None

        for attr in ('min_length', 'max_length', 'unique', 'value_type'):
            if attr in kw: del kw[attr]
        super(Deputies, self).__init__(**kw)


class LegisSession(Field):
    implements(ILegisSession, IChoice)

    vocabularyName = 'XXX' # fake attribute, this allows exportmodel to work

    def __init__(self, **kw):
        self.required = False
        self.vocabulary = LegislativeSessionsSource()
        for attr in ('vocabulary', 'vocabularyName'):
            if attr in kw: del kw[attr]
        super(LegisSession, self).__init__(**kw)

    source = property(lambda self: self.vocabulary)

    def bind(self, object):
        """See zope.schema._bootstrapinterfaces.IField."""
        clone = super(LegisSession, self).bind(object)
        clone.vocabulary = LegislativeSessionsSource()(object)
        return clone

    def _validate(self, value):
        super(LegisSession, self)._validate(value)
        vocabulary = self.vocabulary
        if value not in vocabulary:
            raise ConstraintNotSatisfied(value)

    def fromUnicode(self, str):
        self.validate(str)
        return str



class Ministry(Field):
    implements(IMinistry, IChoice)

    vocabularyName = 'XXX' # fake attribute, this allows exportmodel to work

    def __init__(self, **kw):
        self.required = False
        self.vocabulary = MinistriesSource()
        for attr in ('vocabulary', 'vocabularyName'):
            if attr in kw: del kw[attr]
        super(Ministry, self).__init__(**kw)

    source = property(lambda self: self.vocabulary)

    def bind(self, object):
        """See zope.schema._bootstrapinterfaces.IField."""
        clone = super(Ministry, self).bind(object)
        clone.vocabulary = MinistriesSource()(object)
        return clone

    def _validate(self, value):
        super(Ministry, self)._validate(value)
        vocabulary = self.vocabulary
        if value not in vocabulary:
            raise ConstraintNotSatisfied(value)

    def fromUnicode(self, str):
        self.validate(str)
        return str


class Ministries(Field):
    implements(IMinistries, IList)
    _type = list

    def __init__(self, **kw):
        self.required = False
        self.value_type = SourcedChoice(source=MinistriesSource())
        self.unique = True
        self.min_length = None
        self.max_length = None

        for attr in ('min_length', 'max_length', 'unique', 'value_type'):
            if attr in kw: del kw[attr]
        super(Ministries, self).__init__(**kw)



class DeputyOrMinistry(Field):
    implements(IDeputyOrMinistry, IChoice)

    vocabularyName = 'XXX' # fake attribute, this allows exportmodel to work

    def __init__(self, **kw):
        self.required = False
        self.vocabulary = DeputiesAndMinistriesSource()
        for attr in ('vocabulary', 'vocabularyName'):
            if attr in kw: del kw[attr]
        super(DeputyOrMinistry, self).__init__(**kw)

    source = property(lambda self: self.vocabulary)

    def bind(self, object):
        """See zope.schema._bootstrapinterfaces.IField."""
        clone = super(DeputyOrMinistry, self).bind(object)
        clone.vocabulary = DeputiesAndMinistriesSource()(object)
        return clone

    def _validate(self, value):
        super(DeputyOrMinistry, self)._validate(value)
        vocabulary = self.vocabulary
        if value not in vocabulary:
            raise ConstraintNotSatisfied(value)

    def fromUnicode(self, str):
        self.validate(str)
        return str


class DeputiesOrMinistries(Field):
    implements(IDeputiesOrMinistries, IList)
    _type = list

    def __init__(self, **kw):
        self.required = False
        self.value_type = SourcedChoice(source=DeputiesAndMinistriesSource())
        self.unique = True
        self.min_length = None
        self.max_length = None

        for attr in ('min_length', 'max_length', 'unique', 'value_type'):
            if attr in kw: del kw[attr]
        super(DeputiesOrMinistries, self).__init__(**kw)



class Speakers(Field):
    implements(ISpeakers, IList)
    _type = list

    def __init__(self, **kw):
        self.required = False
        self.value_type = SourcedChoice(source=SpeakersSource())
        self.unique = True
        self.min_length = None
        self.max_length = None

        for attr in ('min_length', 'max_length', 'unique', 'value_type'):
            if attr in kw: del kw[attr]
        super(Speakers, self).__init__(**kw)


class DateOnly(Field):
    implements(IDateOnly)
    min = None
    max = None


class RadioChoice(Choice):
    implements(IRadioChoice)


class RelatedDoc(Relation):
    implements(IRelatedDoc)

    def __init__(self, **kw):
        super(RelatedDoc, self).__init__(**kw)
        self.vocabulary = RelatedDocObjPathSourceBinder()

    source = property(lambda self: self.vocabulary)

    def bind(self, object):
        """See zope.schema._bootstrapinterfaces.IField."""
        clone = super(RelatedDoc, self).bind(object)
        clone.vocabulary = RelatedDocObjPathSourceBinder()(object)
        return clone


class RelatedDocs(RelationList):
    implements(IRelatedDocs)

    def __init__(self, value_type=None, unique=False, **kw):
        if value_type is None:
            value_type = RelatedDoc()
        super(RelatedDocs, self).__init__(value_type=value_type, unique=unique, **kw)


class MailId(TextLine):
    implements(IMailId, IFromUnicode)


class MailRefId(TextLine):
    implements(IMailRefId, IFromUnicode)


class DocHistoLine(Field):
    implements(IDocHistoLine, IFromUnicode)
    date = None
    comment = None

class DocHistoLines(Field):
    implements(IDocHistoLines, IList)
    _type = list

    def __init__(self, **kw):
        self.required = False
        self.value_type = DocHistoLine()
        self.unique = True
        self.min_length = None
        self.max_length = None

        for attr in ('min_length', 'max_length', 'unique', 'value_type'):
            if attr in kw: del kw[attr]
        super(DocHistoLines, self).__init__(**kw)

class PreviewDoc(Field):
    implements(IPreviewDoc, IFromUnicode)

    attribute_name = None

    def __init__(self, **kw):
        for attr in ('height', 'width', 'attribute_name'):
            if kw.get(attr):
                setattr(self, attr, kw.get('attr'))
            if attr in kw:
                del kw[attr]
        super(PreviewDoc, self).__init__(**kw)

class PointingDocs(Field):
    implements(IPointingDocs, IFromUnicode)
