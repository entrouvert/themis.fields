try:
    from plone.supermodel.exportimport import BaseHandler, ChoiceHandler
    from plone.supermodel.fields import ListHandler
    HAVE_SUPERMODEL = True
except ImportError:
    HAVE_SUPERMODEL = False

if HAVE_SUPERMODEL:

    from zope.interface import implements
    from zope.component import adapts
    from plone.supermodel.interfaces import IToUnicode
    from plone.app.relationfield.widget import RelationChoiceHandler

    from themis.fields import Commission, Commissions
    from themis.fields import DateOnly
    from themis.fields import RadioChoice
    from themis.fields import Subjects
    from themis.fields import Deputy, Deputies
    from themis.fields import Ministry, Ministries
    from themis.fields import DeputyOrMinistry, DeputiesOrMinistries
    from themis.fields import Speakers
    from themis.fields import Contact, Contacts
    from themis.fields import LegisSession
    from themis.fields import RelatedDoc
    from themis.fields import RelatedDocs
    from themis.fields import MailId
    from themis.fields import MailRefId
    from themis.fields import DocHistoLines
    from themis.fields import PreviewDoc
    from themis.fields import PointingDocs

    class CommissionHandler_(BaseHandler):
        pass

    CommissionHandler = CommissionHandler_(Commission)

    class DateOnlyHandler_(BaseHandler):
        pass

    DateOnlyHandler = DateOnlyHandler_(DateOnly)

    class SubjectsHandler_(BaseHandler):
        pass

    SubjectsHandler = SubjectsHandler_(Subjects)

    class CommissionsHandler_(BaseHandler):
        pass

    CommissionsHandler = CommissionsHandler_(Commissions)

    class ContactHandler_(BaseHandler):
        pass

    ContactHandler = ContactHandler_(Contact)

    class ContactsHandler_(BaseHandler):
        pass

    ContactsHandler = ContactsHandler_(Contacts)

    class DeputyHandler_(BaseHandler):
        pass

    DeputyHandler = DeputyHandler_(Deputy)

    class DeputiesHandler_(BaseHandler):
        pass

    DeputiesHandler = DeputiesHandler_(Deputies)

    class LegisSessionHandler_(BaseHandler):
        pass

    LegisSessionHandler = LegisSessionHandler_(LegisSession)

    class MinistryHandler_(BaseHandler):
        pass

    MinistryHandler = MinistryHandler_(Ministry)

    class MinistriesHandler_(BaseHandler):
        pass

    MinistriesHandler = MinistriesHandler_(Ministries)

    class DeputyOrMinistryHandler_(BaseHandler):
        pass

    DeputyOrMinistryHandler = DeputyOrMinistryHandler_(DeputyOrMinistry)

    class DeputiesOrMinistriesHandler_(BaseHandler):
        pass

    DeputiesOrMinistriesHandler = DeputiesOrMinistriesHandler_(DeputiesOrMinistries)

    class SpeakersHandler_(BaseHandler):
        pass

    SpeakersHandler = SpeakersHandler_(Speakers)

    class RadioChoiceHandler_(ChoiceHandler):
        pass

    RadioChoiceHandler = RadioChoiceHandler_(RadioChoice)

    class RelatedDocHandler_(BaseHandler):
        pass

    RelatedDocHandler = RelatedDocHandler_(RelatedDoc)

    class RelatedDocsHandler_(BaseHandler):
        pass

    RelatedDocsHandler = RelatedDocsHandler_(RelatedDocs)

    class MailIdHandler_(BaseHandler):
        pass

    MailIdHandler = MailIdHandler_(MailId)

    class MailRefIdHandler_(BaseHandler):
        pass

    MailRefIdHandler = MailRefIdHandler_(MailRefId)

    class DocHistoLinesHandler_(BaseHandler):
        pass

    DocHistoLinesHandler = DocHistoLinesHandler_(DocHistoLines)

    class PreviewDocHandler_(BaseHandler):
        pass

    PreviewDocHandler = PreviewDocHandler_(PreviewDoc)

    class PointingDocsHandler_(BaseHandler):
        pass

    PointingDocsHandler = PointingDocsHandler_(PointingDocs)
