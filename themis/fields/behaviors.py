from zope.interface import Interface
from zope.interface import implements, alsoProvides
from zope.component import adapts


class ITopButtons(Interface):
    pass

class ITopButtonsMarker(Interface):
    pass

class TopButtons(object):
    implements(ITopButtons)
    adapts(ITopButtonsMarker)

