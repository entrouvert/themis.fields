from zope.interface import Interface

from zope.schema.interfaces import IChoice, IField, IList, IDate
from zope import schema
from z3c.form.interfaces import IWidget, IOrderedSelectWidget, ITextWidget
from z3c.relationfield.interfaces import IRelation, IRelationList
from plone.formwidget.autocomplete.interfaces import IAutocompleteWidget

from zope.i18nmessageid import MessageFactory

_ = MessageFactory(u'themis.fields')

class ICommission(IField):
    '''Field containing a unicode string without newlines that is a commission.'''

class ICommissions(IField):
    '''Field contained a list of commissions'''

class ISubjects(IField):
    '''Field contained a list of subjects'''

class IDeputy(IField):
    '''Field containing a deputy'''

class IDeputies(IList):
    '''Field containing a list of deputies'''

class IContact(IField):
    '''Field containing a contact'''

class IContacts(IList):
    '''Field containing a list of contacts'''

class ILegisSession(IField):
    '''Field containing a legislative session'''

class IMinistry(IField):
    '''Field containing a ministry'''

class IMinistries(IList):
    '''Field containing a list of ministries'''

class IDeputyOrMinistry(IField):
    '''Field containing a deputy or a ministry'''

class IDeputiesOrMinistries(IList):
    '''Field containing deputies and/or ministries'''

class ISpeakers(IList):
    '''Field containing a list of speakers'''

class IDateOnly(IDate):
    '''Field containing a date'''
    default = schema.Date(
        title=IDate['default'].title,
        description=IDate['default'].description,
        required=False)

    missing_value = schema.Date(
        title=IDate['missing_value'].title,
        description=IDate['missing_value'].description,
        required=False)

    min = schema.Date(
        title=IDate['min'].title,
        required=IDate['min'].required,
        default=IDate['min'].default,
        )

    max = schema.Date(
        title=IDate['max'].title,
        required=IDate['max'].required,
        default=IDate['max'].default,
        )

class IOrderedSelectAndAddWidget(IOrderedSelectWidget):
    """Ordered Select widget with ITerms option."""

class IRadioChoice(IChoice):
    '''Field containing a single choice, to be presented as radio buttons'''

    default = schema.Choice(
        title=IChoice['default'].title,
        description=IChoice['default'].description,
        required=False,
        values=[])

    missing_value = schema.Choice(
        title=IChoice['missing_value'].title,
        description=IChoice['missing_value'].description,
        required=False,
        values=[])

class IRelatedDoc(IRelation):
    default = schema.Text(
        title=IField['default'].title,
        description=IField['default'].description,
        required=False)

    missing_value = schema.Text(
        title=IField['missing_value'].title,
        description=IField['missing_value'].description,
        required=False)


class IRelatedDocs(IRelationList):
    default = schema.Text(
        title=IField['default'].title,
        description=IField['default'].description,
        required=False)

    missing_value = schema.Text(
        title=IField['missing_value'].title,
        description=IField['missing_value'].description,
        required=False)




class IMailId(IField):
    default = schema.Text(
        description=IField['default'].description,
        required=False)

    missing_value = schema.Text(
        title=IField['missing_value'].title,
        description=IField['missing_value'].description,
        required=False)

class IMailIdWidget(ITextWidget):
    pass

class IMailRefId(IField):
    default = schema.Text(
        description=IField['default'].description,
        required=False)

    missing_value = schema.Text(
        title=IField['missing_value'].title,
        description=IField['missing_value'].description,
        required=False)

class IMailRefIdWidget(ITextWidget):
    pass

class IRelatedDocWidget(IAutocompleteWidget):
    pass

class IRelatedDocsWidget(IAutocompleteWidget):
    pass


class IDocHistoLine(IField):
    date = schema.Date(title=u'Date', required=True)
    comment = schema.TextLine(title=u'Commentaire', required=True)

class IDocHistoLines(IList):
    pass

class IDocHistoLineWidget(IWidget):
    pass

class IPreviewDoc(IField):
    default = schema.Text(
        description=IField['default'].description,
        required=False)

    missing_value = schema.Text(
        title=IField['missing_value'].title,
        description=IField['missing_value'].description,
        required=False)

    required = schema.Bool(
        title=IField['required'].title,
        description=IField['required'].description,
        default=False)

    attribute_name = schema.TextLine(
        title=u'Attribute Name',
        description=u'Attribute Name',
        required=True,
        default=u'fichier')

    width = schema.TextLine(
        title=u'Width',
        required=False)

    height = schema.TextLine(
        title=u'Height',
        required=False)

class IPreviewDocWidget(IWidget):
    pass


class IPointingDocs(IField):
    '''Field to display a list of documents pointing to the current one'''
    required = schema.Bool(
        title=IField['required'].title,
        description=IField['required'].description,
        default=False)

    default = schema.TextLine(
        description=IField['default'].description,
        required=False)

    missing_value = schema.TextLine(
        title=IField['missing_value'].title,
        description=IField['missing_value'].description,
        required=False)


class IPointingDocsWidget(IWidget):
    pass

